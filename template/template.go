package template

import (
	"bytes"
	"html/template"
	"strings"
)

type Plan struct {
	Features    *Priorities
	Bugs        *Priorities
	Maintenance *Priorities
	Blocking    []*Issue
	Warnings    []string
}

var planTemplate = `
{{ define "PlanTemplate" }}
{{ if .Features }}
  # :star2: Features
  {{ template "PrioritiesTemplate" .Features }}
{{ end }}
{{ if .Bugs }}
  # :rotating_light: Bugs
  {{ template "PrioritiesTemplate" .Bugs }}
{{ end }}
{{ if .Maintenance }}
  # :construction: Maintenance
  {{ template "PrioritiesTemplate" .Maintenance }}
{{ end }}
{{ if .Blocking }}
  # Blocking
  {{ range $issue := .Blocking }}
    {{ template "IssueTemplate" . }}
  {{ end }}
{{ end }}
# Warnings
{{ if .Warnings }}
  {{ range $warn := .Warnings }}
  - {{ $warn }}
  {{ end }}
{{ end }}
{{ end }}
`

type Priorities struct {
	P1 Issues
	P2 Issues
	P3 Issues
	P4 Issues
	P5 Issues
}

var prioritiesTemplate = `
{{ define "PrioritiesTemplate" }}
{{ if .P1.Issues }} 
  ~"Runner::P1"
  {{ template "IssuesTemplate" .P1 }}
{{ end }}
{{ if .P2.Issues }}
  ~"Runner::P2"
  {{ template "IssuesTemplate" .P2 }}
{{ end }}
{{ if .P3.Issues }}
  ~"Runner::P3"
  {{ template "IssuesTemplate" .P3 }}
{{ end }}
{{ if .P4.Issues }}
  ~"Runner::P4"
  {{ template "IssuesTemplate" .P4 }}
{{ end }}
{{ if .P5.Issues }}
  ~"Runner::P5"
  {{ template "IssuesTemplate" .P5 }}
{{ end }}
{{ end }}
`

type Issues struct {
	Issues []*Issue
}

var issuesTemplate = `
{{ define "IssuesTemplate" }}
{{ range $issue := .Issues }}
{{ template "IssueTemplate" . }}
{{ end}}
{{ end }}
`

type Issue struct {
	Title       string
	WebURL      string
	Priority    int
	Milestone   string
	Deliverable bool
}

var issueTemplate = `
{{ define "IssueTemplate" }}
- {{ .WebURL }}+s
{{ end }}
`

func (p *Plan) String() string {
	tmpl := template.Must(template.New("").Parse(planTemplate))
	tmpl = template.Must(tmpl.Parse(prioritiesTemplate))
	tmpl = template.Must(tmpl.Parse(issuesTemplate))
	tmpl = template.Must(tmpl.Parse(issueTemplate))
	buf := &bytes.Buffer{}
	tmpl.ExecuteTemplate(buf, "PlanTemplate", p)
	return removeExtraSpace(string(buf.Bytes()))
}

// removeExtraSpace does a little cleanup so the templates can be
// formatted in a more readable fashion.
func removeExtraSpace(s string) string {
	buf := strings.Builder{}
	lineBeginning := true
	newlineCount := 0
	for _, r := range s {
		switch r {
		case '\n':
			lineBeginning = true
			// Remove double new lines
			if newlineCount < 2 {
				buf.WriteRune(r)
				newlineCount++
			}
		case ' ':
			newlineCount = 0
			// Remove leading spaces
			if !lineBeginning {
				buf.WriteRune(r)
			}
		default:
			lineBeginning = false
			newlineCount = 0
			buf.WriteRune(r)
		}
	}
	return buf.String()
}
