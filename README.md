Plan-me automatically creates an iteration plan for a specific group. It fetches a milestone and a list of issues, groups them by type and priority, and prints a plan, summary, and oversight information.

# Supported features

- Generating iteration plan as text output for group Runner SaaS and it's specific priority labels
- Adding blocking issues of all planned issues
- Warning for blocking issue with a later milestone or missing priority
- Adding any related issues of all planned issues with a matching milestone

# Features coming next

- Generation of a new iteration plan issue for a given milestone
- Updating an existing iteration plan issue
- Dynamic input of group & priority labels
- Automation to run every month and automatically generate the issue X days before the iteration starts
- Tagging PM's X days before iteration to review & adjust
- Tagging engineering team X days before an iteration to review the plan
- Sum up weights of planned issues
- Compare weights with average completed weights of past iterations
- Generate report of scope change within an iteration

# Usage

Run `plan-me 16.1` (or `go run . 16.1`) and get output like this:

```
### Runner SaaS :cloud:

### feature

~"Runner::P1"

- https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29405+s

~"Runner::P2"

- https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/75+s
- https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/50+s
- https://gitlab.com/gitlab-org/gitlab/-/issues/392922+s
- https://gitlab.com/gitlab-org/gitlab/-/issues/388166+s
- https://gitlab.com/gitlab-org/gitlab/-/issues/388165+s
- https://gitlab.com/gitlab-org/gitlab/-/issues/384223+s

### maintenance

~"Runner::P2"

- https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29823+s
- https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/60+s

### bug

~"Runner::P1"

- https://gitlab.com/gitlab-org/fleeting/taskscaler/-/issues/10+s
- https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/77+s
- https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/79+s

~"Runner::P2"

- https://gitlab.com/gitlab-org/ci-cd/shared-runners/macos/-/issues/24+s
- https://gitlab.com/gitlab-org/gitlab/-/issues/379469+s
- https://gitlab.com/gitlab-org/gitlab/-/issues/379474+s

### Related issues in this milestone

- <issues go here when there are some>

### Summary

- if we complete all P1 issues then the `say-do` ratio will be 10.81%
- if we complete all P2+ issues then the `say-do` ratio will be 59.46%
- if we complete all issues then the `say-do` ratio will be 59.46%

### Oversight

- there are no ~"Deliverable"s this milestone
- issue https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5401+s which blocks https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/60+s has no priority
- blocking issue https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5401+s is not in the same milestone as https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/60+s
- issue https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/59+s which blocks https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/60+s has no priority
- blocking issue https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/59+s is not in the same milestone as https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/60+s
```
