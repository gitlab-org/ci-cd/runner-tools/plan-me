package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/plan-me/pkg/plan"
)

var (
	milestone   = flag.String("milestone", "", "Milestone")
	outputJson  = flag.Bool("json", false, "Output plan as JSON")
	project     = flag.String("project", "", "Project")
	planIssueID = flag.Int("plan-issue-id", 0, "Existing milestone issue ID")
)

type app struct {
	client    *gitlab.Client
	milestone string
}

func main() {
	flag.Parse()

	if len(flag.Args()) != 1 {
		panic(fmt.Sprintf("usage: plan-me <command> [flags]. got %v", flag.Args()))
	}

	client, err := makeClient()
	milestone, err := getMilestone()
	if err != nil {
		panic(err)
	}
	a := &app{
		client:    client,
		milestone: milestone,
	}

	switch flag.Args()[0] {
	case "print":
		err = a.print()
	case "lint":
		err = a.lint()
	default:
		panic("usage: plan-me <command> [flags]")
	}
	if err != nil {
		panic(err)
	}
}

func (a *app) print() error {
	mePlan, err := plan.BuildPlan(a.client, plan.Options{Milestone: a.milestone})
	if err != nil {
		return err
	}
	if *outputJson {
		planJson, err := json.Marshal(mePlan)
		if err != nil {
			return err
		}
		fmt.Println(string(planJson))
		return nil
	}
	fmt.Println(mePlan.String())
	return nil
}

func (a *app) lint() error {
	if *project == "" {
		return fmt.Errorf("--project required")
	}
	if *planIssueID == 0 {
		return fmt.Errorf("--plan-issue-id required")
	}
	issue, _, err := a.client.Issues.GetIssue(*project, *planIssueID)
	if err != nil {
		return err
	}
	planText := issue.Description
	return plan.Lint(a.client, plan.Options{Milestone: a.milestone}, planText)
}

func makeClient() (*gitlab.Client, error) {
	token := os.Getenv("GL_TOKEN")
	return gitlab.NewClient(token)
}

func getMilestone() (string, error) {
	if *milestone == "" {
		return "", fmt.Errorf("--milestone required")
	}
	fields := strings.Split(*milestone, ".")
	// Validate milestone
	if len(fields) != 2 {
		return "", fmt.Errorf("invalid milestone: %v", milestone)
	}
	if _, err := strconv.Atoi(fields[0]); err != nil {
		return "", fmt.Errorf("invalid milestone major version: %v", milestone)
	}
	if _, err := strconv.Atoi(fields[1]); err != nil {
		return "", fmt.Errorf("invalid milestone minor version: %v", milestone)
	}
	return *milestone, nil
}
