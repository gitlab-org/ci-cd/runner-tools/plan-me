package plan

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/plan-me/template"
	"mvdan.cc/xurls/v2"
)

type Options struct {
	Milestone string
}

type warnings []string

func BuildPlan(client *gitlab.Client, options Options) (*template.Plan, error) {

	plan := &template.Plan{}
	milestoneIssues, err := listIssues(client, options)
	if err != nil {
		return nil, err
	}

	issuesByType, err := groupByLabel(milestoneIssues, "type")
	if err != nil {
		return nil, err
	}

	ignore := &template.Priorities{}
	issuesWithType := map[int]*gitlab.Issue{}
	for typeString, priorities := range map[string]**template.Priorities{
		"feature":     &plan.Features,
		"bug":         &plan.Bugs,
		"maintenance": &plan.Maintenance,
		"ignore":      &ignore,
	} {
		typeIssues, _ := issuesByType[typeString]
		for _, i := range typeIssues {
			issuesWithType[i.ID] = i
		}
		if typeString == "ignore" {
			continue
		}
		p, warnings, err := buildPriorities(typeIssues)
		if err != nil {
			return nil, err
		}
		*priorities = p
		plan.Warnings = append(plan.Warnings, warnings...)
	}

	issuesWithoutType := map[int]*gitlab.Issue{}
	for _, i := range milestoneIssues {
		if _, ok := issuesWithType[i.ID]; !ok {
			issuesWithoutType[i.ID] = i
		}
	}
	for _, i := range issuesWithoutType {
		plan.Warnings = append(plan.Warnings, fmt.Sprintf("Issue %v has no type", i.WebURL))
	}

	return plan, nil
}

func Lint(client *gitlab.Client, options Options, planText string) error {
	planIssues, err := issuesFromPlan(client, planText)
	if err != nil {
		return err
	}
	planIssuesSet := map[int]*gitlab.Issue{}
	for _, i := range planIssues {
		planIssuesSet[i.IID] = i
	}
	milestoneIssues, err := listIssues(client, options)
	if err != nil {
		return err
	}
	milestoneIssuesSet := map[int]*gitlab.Issue{}
	for _, i := range milestoneIssues {
		milestoneIssuesSet[i.IID] = i
	}

	// All issues in the plan should have priority
	for _, i := range planIssues {
		labels := i.Labels
		priority, err := getLabel(labels, "Runner")
		if err != nil {
			return err
		}
		if priority == "" {
			fmt.Printf("Issue %v has no Runner priority label\n", i.WebURL)
		}
	}

	// All issues in the plan should be in the canonical milestone
	for _, i := range planIssues {
		if _, ok := milestoneIssuesSet[i.IID]; !ok {
			fmt.Printf("Issue %v is in the plan but not the milestone\n", i.WebURL)
		}
	}

	// All issues in the canonical milestone should be in the plan
	for _, i := range milestoneIssues {
		if _, ok := planIssuesSet[i.IID]; !ok {
			fmt.Printf("Issue %v is in the milestone but not in the plan\n", i.WebURL)
		}
	}

	// Print summary statistics
	fmt.Println("SUMMARY")
	planIssuesByLabel, err := groupByLabel(planIssues, "Runner")
	if err != nil {
		return err
	}
	for priority, issues := range planIssuesByLabel {
		if priority == "" {
			fmt.Printf("%v issues have no priority\n", len(issues))
		} else {
			fmt.Printf("%v %v issues\n", len(issues), priority)
		}
	}

	return nil
}

func issuesFromPlan(client *gitlab.Client, planText string) ([]*gitlab.Issue, error) {
	issueURLs := []string{}
	rxStrict := xurls.Strict()
	for _, line := range strings.Split(planText, "\n") {
		urls := rxStrict.FindAllString(line, 1)
		if urls == nil || len(urls) == 0 {
			continue
		}
		if strings.Contains(urls[0], "?") {
			// Skip queries
			continue
		}
		if !strings.Contains(urls[0], "issues") {
			// Skip epics, work items, boards, etc...
			continue
		}
		issueURLs = append(issueURLs, urls[0])
	}
	issues := []*gitlab.Issue{}
	for _, i := range issueURLs {
		project, issueID, err := projectIssueFromURL(i)
		if err != nil {
			return nil, err
		}
		issue, _, err := client.Issues.GetIssue(project, issueID)
		if err != nil {
			return nil, err
		}
		issues = append(issues, issue)
	}
	return issues, nil
}

func projectIssueFromURL(url string) (string, int, error) {
	invalidErr := fmt.Errorf("invalid URL: %v", url)
	rest, found := strings.CutPrefix(url, "https://gitlab.com/")
	if !found {
		return "", 0, invalidErr
	}
	fields := strings.Split(rest, "/")
	if len(fields) < 2 {
		return "", 0, invalidErr
	}
	idString := fields[len(fields)-1]
	idString, _ = strings.CutSuffix(idString, "+s")
	id, err := strconv.Atoi(idString)
	if err != nil {
		return "", 0, invalidErr
	}
	project := strings.Join(fields[:len(fields)-2], "/")
	project, _ = strings.CutSuffix(project, "/-")
	return project, id, nil
}

func buildPriorities(typeIssues []*gitlab.Issue) (*template.Priorities, warnings, error) {
	var warnings warnings
	issuesByPriority, err := groupByLabel(typeIssues, "Runner")
	if err != nil {
		return nil, nil, err
	}
	templatePriorities := &template.Priorities{}

	issuesWithPriority := map[int]*gitlab.Issue{}
	for value, templateIssues := range map[string]*template.Issues{
		"P1": &templatePriorities.P1,
		"P2": &templatePriorities.P2,
		"P3": &templatePriorities.P3,
		"P4": &templatePriorities.P4,
		"P5": &templatePriorities.P5,
	} {
		if priorityIssues, ok := issuesByPriority[value]; ok {
			for _, i := range priorityIssues {
				issuesWithPriority[i.ID] = i
			}
			issues, w, err := buildIssues(priorityIssues)
			if err != nil {
				return nil, nil, err
			}
			*templateIssues = *issues
			warnings = append(warnings, w...)
		}
	}

	issuesWithoutPriority := map[int]*gitlab.Issue{}
	for _, i := range typeIssues {
		if _, ok := issuesWithPriority[i.ID]; !ok {
			issuesWithoutPriority[i.ID] = i
		}
	}
	for _, i := range issuesWithoutPriority {
		warnings = append(warnings, fmt.Sprintf("Issue %v has no priority", i.WebURL))
	}

	return templatePriorities, warnings, nil
}

func buildIssues(priorityIssues []*gitlab.Issue) (*template.Issues, warnings, error) {
	templateIssues := &template.Issues{}

	for _, i := range priorityIssues {
		templateIssues.Issues = append(templateIssues.Issues, &template.Issue{
			Title:  i.Title,
			WebURL: i.WebURL,
			// TODO get priority
			Milestone: i.Milestone.Title,
			// TODO get deliverable
		})
	}

	return templateIssues, nil, nil
}

func listIssues(client *gitlab.Client, options Options) ([]*gitlab.Issue, error) {
	labels := []string{
		"Category:Runner SaaS",
	}
	scope := "all"
	issues, _, err := client.Issues.ListIssues(&gitlab.ListIssuesOptions{
		Milestone: &options.Milestone,
		Labels:    (*gitlab.Labels)(&labels),
		Scope:     &scope,
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
		},
	})
	if err != nil {
		return nil, err
	}
	return issues, nil
}

func groupByLabel(issues []*gitlab.Issue, labelKey string) (map[string][]*gitlab.Issue, error) {
	group := map[string][]*gitlab.Issue{}
	for _, i := range issues {
		v, err := getLabel(i.Labels, labelKey)
		if err != nil {
			return nil, err
		}
		group[v] = append(group[v], i)
	}
	return group, nil
}

func getLabel(labels gitlab.Labels, labelKey string) (string, error) {
	for _, l := range labels {
		fields := strings.Split(l, "::")
		if len(fields) != 2 {
			continue
		}
		k, v := fields[0], fields[1]
		if k != labelKey {
			continue
		}
		return v, nil
	}
	return "", nil
}
